package main

import (
	"fmt"
	"log"

	service "github.com/ayufan/golang-kardianos-service"
)

var logger service.Logger

type program struct{}

func (p *program) Start(s service.Service) error {
	// Start should not block. Do the actual work async.
	// (https://github.com/kardianos/service/blob/master/service.go#L312)

	fmt.Println("Starting the daemon")

	go p.run()
	return nil
}

func (p *program) run() {
	// Do work here

	fmt.Println("Starting run()")

	err := StartRPCServer("0.0.0.0:50051")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Finished run()")
}

func (p *program) Stop(s service.Service) error {
	// Stop should not block. Return with a few seconds.
	// (https://github.com/kardianos/service/blob/master/service.go#L312)

	fmt.Println("Stopping the daemon")

	return nil
}

func main() {
	svcConfig := &service.Config{
		Name:        "Testing service",
		DisplayName: "Go Service Test",
		Description: "This is just for testing",
	}

	prg := &program{}
	s, err := service.New(prg, svcConfig)
	if err != nil {
		log.Fatal(err)
	}
	logger, err = s.Logger(nil)
	if err != nil {
		log.Fatal(err)
	}
	err = s.Run()
	if err != nil {
		logger.Error(err)
	}
}

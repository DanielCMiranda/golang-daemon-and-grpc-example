package main

import (
	"context"
	"fmt"
	"net"

	"google.golang.org/grpc"

	"golang-daemon-example/proto"
)

type server struct {
}

// Hello implements the handler for the message received by the client (defined in the proto file)
func (*server) Hello(ctx context.Context, request *proto.ExampleRequest) (*proto.ExampleResponse, error) {
	name := request.Name
	response := &proto.ExampleResponse{
		Greeting: "Hello " + name,
	}
	return response, nil
}

// StartRPCServer starts the RPC server using the specified address
func StartRPCServer(address string) error {
	lis, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}
	fmt.Printf("Server is listening on %v ...", address)

	g := grpc.NewServer()
	proto.RegisterExampleServiceServer(g, &server{})

	err = g.Serve(lis)

	return err
}

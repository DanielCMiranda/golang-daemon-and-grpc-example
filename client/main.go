package main

import (
	"context"
	"fmt"
	"log"

	"google.golang.org/grpc"

	"golang-daemon-example/proto"
)

func main() {
	fmt.Println("Hello client ...")

	opts := grpc.WithInsecure()
	cc, err := grpc.Dial("localhost:50051", opts)
	if err != nil {
		log.Fatal(err)
	}
	defer cc.Close()

	client := proto.NewExampleServiceClient(cc)
	request := &proto.ExampleRequest{Name: "John"}

	resp, _ := client.Hello(context.Background(), request)
	fmt.Printf("Receive response => [%v]", resp.Greeting)
}
